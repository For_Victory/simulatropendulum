# Pendulum simulator

**Find issue?** [Tell me](https://bitbucket.org/For_Victory/simulatropendulum/issues/new)

for migration:
add apps to settings INSTALLED_APPS

for activating models:
python manage.py makemigrations pendulums

for showing running code:
python manage.py sqlmigrate polls 0001

for running:
python manage.py migrate

# verbose_name

in models - for changing names of fields

# createsuperuse

admin
Qwerty789#

https://docs.djangoproject.com/en/2.0/intro/tutorial02/#creating-an-admin-user

# filter

https://stackoverflow.com/questions/769843/how-do-i-use-and-in-a-django-filter/770078#770078

using Q

# using in javascript

https://stackoverflow.com/questions/298772/django-template-variables-and-javascript
https://code.djangoproject.com/ticket/17419

# disallow not existing mistakes

"python.linting.pylintArgs": [
        "--load-plugins=pylint_django"
    ]

In visual studio code setting


# structure

/pendulums - list of pendulums (check pendulum and go to the page <int:pk>)
/pendulums/create - create pendulum with parametrs
/pendulums/<int:pk> - page of pendulum with drawing

after create we need to run python Pendulum class for generated coordinates

# get values from connected model

```
{% for p in pendulum.coordinates_set.all %}
    <p>{{p.coordinate}}</p>
{% endfor %}
```

# get response in View

self.object.rowname

# values

```
    M = self.object.mass  # mass of cart+pendulum
    m = self.object.mass_of_pendulum  # mass of pendulum
    Km = self.object.torgue_constant  # motor torque constant
    Kg = self.object.gear_ratio  # gear ratio
    R = self.object.resistance  # armiture resistance
    r = self.object.drive_radius  # drive radiu3
    K1 = Km*Kg/(R*r)
    K2 = Km**2*Kg**2/(R*r**2)
    l = self.object.length_pendulum # length of pendulum to CG
    I = self.object.inertia  # inertia of the pendulum
    L = (I + m*l**2)/(m*l)
    g = self.object.gravity  # gravity
    Vsat = self.object.voltage  # saturation voltage
```

# test

from default
1. M 0.6 >= 0.7 +
2. m 0.3 >= 0.4 + 
m 0.5 + (не может поднять)
3. Km 2 >= 3 +
4. Kg 0.01 >= 0.015
5. R 6 >= 8
6. r 0.01 >= 0.015
7. l 0.3 >= 0.5
8. I 0.006 to 0.002
9. g 9.81 to 20


# deploy

[deploy instructions (Ubuntu 16.04)](https://www.digitalocean.com/community/tutorials/how-to-install-django-and-set-up-a-development-environment-on-ubuntu-16-04)

[serve with Gunicorn](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-16-04#create-a-gunicorn-systemd-service-file)

Всё под sudo!

Склонировал проект в папку /var/www

```
sudo apt-get install libmysqlclient-dev (if mysql_config not found)
sudo apt-get install python3-tk

sudo pip3 install django
sudo pip3 install numpy
sudo pip3 install mysqlclient
```

Миграция БД:

sudo python3 manage.py migrate

mysql:

```
Create database pendulum;
Insert into `pendulum`.`pendulums_coordinatestypes` VALUES (1, 'каретка'), (2, 'маятник');
```
```
sudo ufw allow 8000 (or another port)  
sudo ufw status verbose

sudo apt-get install gfortran liblapack-dev
sudo pip3 install slycot 
```
(or clone slycot from [bitbucket](https://bitbucket.org/For_Victory/slycot))

В некоторых случаях удалить - `{% csrf_token %}` из simulatropendulum/pendulums/templates/pendulums/detail.html


# slycot issue

https://github.com/python-control/Slycot/issues/15
(lapack not found)
sudo apt-get install gfortran liblapack-dev

or add slycot libraries to 
* /usr/local/lib/python3.5/dist-packages (Ubuntu)
* \Lib\site-packages\ - Windows 10 (conda)  
from [bitbucket_repo](https://bitbucket.org/For_Victory/slycot)

# run server global

cd /var/www/simulatropendulum  

. ./env/bin/activate - to activate virtualenv

sudo python3 manage.py runserver 0.0.0.0:8000

# DEMO

[DEMO](http://159.93.169.250/pendulums/)
