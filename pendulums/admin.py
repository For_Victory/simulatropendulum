from django.contrib import admin
from .models import Pendulum, CoordinatesTypes

admin.site.register(Pendulum)
admin.site.register(CoordinatesTypes)
# Register your models here.
