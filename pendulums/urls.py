from django.urls import path

from . import views
from django import forms

app_name = 'pendulums'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create/', views.PendulumView.as_view(), name='pendulum'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'), 
]
