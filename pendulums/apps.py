from django.apps import AppConfig


class PendulumsConfig(AppConfig):
    name = 'pendulums'
