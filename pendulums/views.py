from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.generic.edit import CreateView
from django.views import generic
from django.forms import ModelForm, Textarea
from django import forms
from .models import Pendulum, Coordinates, CoordinatesTypes
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.urls import reverse
from django.db import transaction
from itertools import islice

from math import sin, cos, pi
from numpy import matrix, array
from control.matlab import lqr

# PENDULUM

#M = .6  # mass of cart+pendulum
#m = .3  # mass of pendulum
#Km = 2  # motor torque constant
#Kg = .01  # gear ratio
#R = 6  # armiture resistance
#r = .01  # drive radiu3
#K1 = Km*Kg/(R*r)
#K2 = Km**2*Kg**2/(R*r**2)
#l = .3  # length of pendulum to CG
#I = 0.006  # inertia of the pendulum
#L = (I + m*l**2)/(m*l)
#g = 9.81  # gravity
#Vsat = 20.  # saturation voltage



# ENDPENDULUM


class IndexView(generic.ListView):
    template_name = 'pendulums/index.html'
    context_object_name = 'latest_pendulums_list'

    def get_queryset(self):
        """
        Return the last ten pendulums
        """
        return Pendulum.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:10]


class PendulumView(CreateView):
    template_name = 'pendulums/pendulum_form.html'
    model = Pendulum
    fields = '__all__'

    # @transaction.commit_manually
    def get_success_url(self):
        """coordinates table filling"""
        M = float(self.object.mass)  # mass of cart+pendulum
        m = float(self.object.mass_of_pendulum)  # mass of pendulum
        Km = float(self.object.torgue_constant)  # motor torque constant
        Kg = float(self.object.gear_ratio)  # gear ratio
        R = float(self.object.resistance)  # armiture resistance
        r = float(self.object.drive_radius)  # drive radiu3
        K1 = Km*Kg/(R*r)
        K2 = Km**2*Kg**2/(R*r**2)
        l = float(self.object.length_pendulum) # length of pendulum to CG
        I = float(self.object.inertia)  # inertia of the pendulum
        L = (I + m*l**2)/(m*l)
        g = float(self.object.gravity)  # gravity
        Vsat = float(self.object.voltage)  # saturation voltage

        A11 = -1 * Km**2*Kg**2 / ((M - m*l/L)*R*r**2)
        A12 = -1*g*m*l / (L*(M - m*l/L))
        A31 = Km**2*Kg**2 / (M*(L - m*l/M)*R*r**2)
        A32 = g/(L-m*l/M)
        A = matrix([
            [0, 1, 0, 0],
            [0, A11, A12, 0],
            [0, 0, 0, 1],
            [0, A31, A32, 0]
        ])

        B1 = Km*Kg/((M - m*l/L)*R*r)
        B2 = -1*Km*Kg/(M*(L-m*l/M)*R*r)

        B = matrix([
            [0],
            [B1],
            [0],
            [B2]
        ])
        Q = matrix([
            [10000, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 10000, 0],
            [0, 0, 0, 1]
        ])

        (K, X, E) = lqr(A, B, Q, R)


        def cmp(a, b):
            return (a > b) - (a < b)


        def constrain(theta):
            theta = theta % (2*pi)
            if theta > pi:
                theta = -2*pi+theta
            return theta


        def sat(Vsat, V):
            if abs(V) > Vsat:
                return Vsat * cmp(V, 0)
            return V


        def average(x):
            x_i, k1, k2, k3, k4 = x
            return x_i + (k1 + 2.0*(k3 + k4) + k2) / 6.0


        theta = []


        class PendulumGen(object):
            def __init__(self, dt, init_conds, end):
                self.dt = dt
                self.t = 0.0
                self.x = init_conds[:]
                self.end = end

            def derivative(self, u):
                V = sat(Vsat, self.control(u))
                # x1 = x, x2 = x_dt, x3 = theta, x4 = theta_dt
                x1, x2, x3, x4 = u
                x1_dt, x3_dt = x2, x4
                x2_dt = (K1*V - K2*x2 - m*l*g*cos(x3)*sin(x3)/L +
                        m*l*sin(x3)*x4**2) / (M - m*l*cos(x3)**2/L)
                x4_dt = (g*sin(x3) - m*l*x4**2*cos(x3)*sin(x3)/L - cos(x3)
                        * (K1*V + K2*x2)/M) / (L - m*l*cos(x3)**2/M)
                x = [x1_dt, x2_dt, x3_dt, x4_dt]
                return x

            def control(self, u):
                c = constrain(u[2])
                if c > -pi/5 and c < pi/5:
                    return float(-K*matrix(u[0:2]+[c]+[u[3]]).T)
                else:
                    return self.swing_up(u)

            def swing_up(self, u):
                E0 = 0.
                k = 1
                w = (m*g*l/(4*I))**(.5)
                E = m*g*l*(.5*(u[3]/w)**2 + cos(u[2])-1)
                a = k*(E-E0)*cmp(u[3]*cos(u[2]), 0)
                F = M*a
                V = (F - K2*constrain(u[2]))/K1
                return sat(Vsat, V)

            def rk4_step(self, dt):
                dx = self.derivative(self.x)
                k2 = [dx_i*dt for dx_i in dx]

                xv = [x_i + delx0_i/2.0 for x_i, delx0_i in zip(self.x, k2)]
                k3 = [dx_i*dt for dx_i in self.derivative(xv)]

                xv = [x_i + delx1_i/2.0 for x_i, delx1_i in zip(self.x, k3)]
                k4 = [dx_i*dt for dx_i in self.derivative(xv)]

                xv = [x_i + delx1_2 for x_i, delx1_2 in zip(self.x, k4)]
                k1 = [self.dt*i for i in self.derivative(xv)]

                self.t += dt
                self.x = list(map(average, zip(self.x, k1, k2, k3, k4)))
                theta.append(constrain(self.x[2]))

            def integrate(self):
                x = []
                while self.t <= self.end:
                    self.rk4_step(self.dt)
                    x.append([self.t] + self.x)
                return array(x)


        pend = PendulumGen(
            float(self.object.dt),
            [0, 0., pi, 0.],
            float(self.object.time),
        )

        data = pend.integrate()

        i = 0

        # with transaction.atomic(): #3.66

        objs = (Coordinates(step=i, coordinate_cart=item[1], coordinates_type_id=1,
                            pendulum_id=self.object.id, coordinate_pendulum=item[3]) for item in data)
        while True:
            batch = list(islice(objs, len(data)))
            if not batch:
                break
            Coordinates.objects.bulk_create(batch, len(data))

           # c =
           # c.save()
           #i += 1
        # transaction.commit

        return reverse('pendulums:detail', args=(self.object.id,))


class DetailView(generic.DetailView):
    template_name = 'pendulums/detail.html'
    model = Pendulum
    #context_object_name = 'pend'

    def get_queryset(self):
        """what"""
        return Pendulum.objects
