from django.db import models
from django.utils.translation import gettext_lazy as _


class Pendulum(models.Model):
    dt = models.DecimalField(
        max_digits=6, decimal_places=5, default=.05,
        verbose_name=_('Шаг времени'))
    time = models.DecimalField(
        max_digits=5, decimal_places=2, default=10,
        verbose_name=_('Время моделирования (сек)'))
    mass = models.DecimalField(
        max_digits=19, decimal_places=10, default=.6,
        verbose_name=_('Масса каретки и маятника'))
    mass_of_pendulum = models.DecimalField(
        max_digits=19, decimal_places=10, default=.3,
        verbose_name=_('Масса маятника'))
    torgue_constant = models.DecimalField(
        max_digits=19, decimal_places=10, default=2,
        verbose_name=_('Постоянная момента двигателя'))
    gear_ratio = models.DecimalField(
        max_digits=19, decimal_places=10, default=.01,
        verbose_name=_('Передаточное число'))
    resistance = models.DecimalField(
        max_digits=19, decimal_places=10, default=6,
        verbose_name=_('Сопротивление'))
    drive_radius = models.DecimalField(
        max_digits=19, decimal_places=10, default=.01,
        verbose_name=_('Радиус'))
    length_pendulum = models.DecimalField(
        max_digits=19, decimal_places=10, default=.3,
        verbose_name=_('Высота маятника'))
    inertia = models.DecimalField(
        max_digits=19, decimal_places=10, default=.006,
        verbose_name=_('Инерция'))
    gravity = models.DecimalField(
        max_digits=19, decimal_places=10, default=9.81,
        verbose_name=_('Гравитация'))
    voltage = models.DecimalField(
        max_digits=19, decimal_places=10, default=20,
        verbose_name=_('Напряжение'))
    pub_date = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания')


class CoordinatesTypes(models.Model):
    type_text = models.CharField(max_length=400)


class Coordinates(models.Model):
    pendulum = models.ForeignKey(Pendulum, on_delete=models.CASCADE)
    step = models.IntegerField()
    coordinates_type = models.ForeignKey(
        CoordinatesTypes, on_delete=models.CASCADE)
    coordinate_cart = models.DecimalField(max_digits=19, decimal_places=17)
    coordinate_pendulum = models.DecimalField(max_digits=19, decimal_places=17)
